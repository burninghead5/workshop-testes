package paginashtml;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import junit.framework.Assert;

public class CampoTreinamento {
	
	@Test
	public void componentes(){
	System.setProperty("webdriver.chrome.driver", "C:/Users/Berzerk/Downloads/chromedriver_win32 (4)/chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("C:/Users/Berzerk/workspace/test/src/main/resources/componentes.html");
	
	WebElement nome = driver.findElement(By.id("elementosForm:nome"));
	nome.sendKeys("Eduardo");
	Assert.assertEquals("Eduardo", driver.findElement(By.id("elementosForm:nome")).getAttribute("value"));
	
	WebElement sexo = driver.findElement(By.id("elementosForm:sexo:0"));
	sexo.click();
	Assert.assertEquals("M", driver.findElement(By.id("elementosForm:sexo:0")).getAttribute("value"));
	
	WebElement comidaFav = driver.findElement(By.id("elementosForm:comidaFavorita:2"));
	comidaFav.click();
	Assert.assertEquals("pizza", driver.findElement(By.id("elementosForm:comidaFavorita:2")).getAttribute("value"));
	
	WebElement escolaridade = driver.findElement(By.id("elementosForm:escolaridade"));
	Select combo = new Select(escolaridade);
	combo.selectByVisibleText("2o grau completo");
	Assert.assertEquals("2o grau completo", combo.getFirstSelectedOption().getText());
	
	WebElement esporte = driver.findElement(By.id("elementosForm:esportes"));
	Select combo1 = new Select(esporte);
	combo1.selectByVisibleText("Corrida");
	Assert.assertEquals("Corrida", combo1.getFirstSelectedOption().getText());
 
	
	
	
	
	
	}

}
